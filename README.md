# Clinostat

Clinostat code for running on ESP32 boards.

Code and schematics for open clinostat.
This is part of the [tranxxeno lab](https://tranxxenolab.net).

# Credits

Thanks to Lovrenc Košenina for his extensive design, fabrication, and electronics assistance. 

Created during a Biofriction residency.

Produced by Galerija Kapelica/Zavod Kersnikova in Ljubljana, Slovenia.

Biofriction is supported by the European CommissionCreative Europe.
