/*
 * Put here specific configuration information
 */

// Include this for the log level definitions
#include <ArduinoLog.h>

// What is our log level?
#define log_level LOG_LEVEL_VERBOSE

// What baud rate do we want for the serial console?
#define SERIAL_BAUD 115200

// What's our clinostat ID?
#define CLINOSTAT_ID 0

// What kind of clinostat are we running? 
enum clinostatKinds {
  CLINOSTAT_LAB = 0,
  CLINOSTAT_SACCULAR_FOUNT = 1
} clinostatKind = CLINOSTAT_LAB;

// Do we have a screen?
#define HAVE_SCREEN 1

// Do we have an accelerometer board?
#define HAVE_ACCEL 1

// Are we using Wifi?
#define USE_WIFI 1

// Time server definitions
#define SET_TIME 1
#define TIME_ZONE "CET-1CEST"
//#define NTP_SERVER "pool.ntp.org"
#define NTP_SERVER "2.europe.pool.ntp.org"

//const char* ntpServer = "pool.ntp.org";
//const long  gmtOffset_sec = 3600;
//const int   daylightOffset_sec = 3600;

void IRAM_ATTR motorEnableHigh();
void IRAM_ATTR motorEnableLow();
