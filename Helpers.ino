// Include this for the log level definitions
#include <ArduinoLog.h>

void outputSPIPins() {
  Log.notice("MOSI: %d" CR, MOSI);
  Log.notice("MISO: %d" CR, MISO);
  Log.notice("SCK: %d" CR, SCK);
  Log.notice("SS: %d" CR, SS);
  //Log.notice("RST: %d" CR, RST);
}

void outputI2CPins() {
  Log.notice("SCL: %d" CR, SCL);
  Log.notice("SDA: %d" CR, SDA);
}
